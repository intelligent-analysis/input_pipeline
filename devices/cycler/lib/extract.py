import dash
import json
import holoviews as hv
import hvplot
import hvplot.pandas
import hvplot.xarray
import pandas as pd
import plotly.io as pio
import seaborn as sns
import yadg
import kadi_apy
import shutil

from holoviews.plotting.plotly.dash import to_dash
from kadi_apy.lib.conversion import json_to_kadi
from pathlib import Path

pd.options.plotting.backend = "holoviews"

metadata_prefix = "metadata"
figure_prefix = "figure"
dashboard_prefix = "dashboard"
summary_prefix = "summary"
auto_key = "auto"
comment_char = "# "


def extract_biologic_mpr(
    file: Path, mass: float = None, filetype: str = "biologic-mpr"
):

    # Settings
    pd.options.plotting.backend = "holoviews"
    hvplot.extension("plotly")
    pio.templates.default = "simple_white"

    # Files
    file = Path(file)
    metadata_file = file.parent / f"{metadata_prefix}_{file.stem}.json"
    summary_file = file.parent / f"{summary_prefix}_{file.stem}"
    figure_file = file.parent / f"{figure_prefix}_{file.stem}"
    # dashboard_file = file.parent / f"{dashboard_prefix}_{file.stem}"
    ds = yadg.extractors.extract(filetype=filetype, path=file)
    ds = ds.drop_indexes("uts")

    # Specific values
    mass = mass or ds.attrs.get("settings", {}).get("active_material_mass", None)
    e_key = "Ecell"
    ds[e_key] = ds["Ewe"] - ds["Ece"]
    q_key = "Q charge or discharge"
    q_charge_key = "Q charge"
    q_discharge_key = "Q discharge"
    if mass is not None:
        ds[q_key.lower()] = ds[q_key] / (mass * 1e-3)  # Convert mass from mg to g
        ds[q_key.lower()].attrs["units"] = ds[q_key].attrs["units"] + "/g"
        q_key = q_key.lower()
        q_charge_key = q_charge_key.lower()
        q_discharge_key = q_discharge_key.lower()

    # Data processing
    ds[q_charge_key] = abs(ds[q_key].where(ds["Ns"] == 1))
    ds[q_discharge_key] = abs(ds[q_key].where(ds["Ns"] == 2))
    # Initial loading e.g. of hard carbon electrodes initial auto loading
    initial_control_current = ds["control_C"].where(ds["half cycle"] == 0).mean()
    if initial_control_current > 0.0:
        ds["cycle"] = ds["half cycle"] // 2 + 1
    else:
        ds["cycle"] = (ds["half cycle"].where(ds["half cycle"] > 0) + 1) // 2
    ds["cycle"] = ds["cycle"].fillna(1).astype(int)

    # Compute characteristic properties
    ds_cycles = [df_c for _, df_c in ds.groupby("cycle")]
    cycle_properties = [
        dict(
            capacity_charge=float(dsc[q_charge_key].max() - dsc[q_charge_key].min()),
            capacity_discharge=float(
                dsc[q_discharge_key].max() - dsc[q_discharge_key].min()
            ),
        )
        for dsc in ds_cycles
    ]
    properties_df = pd.DataFrame.from_records(cycle_properties)
    properties_df["coulombic_efficiency"] = (
        properties_df["capacity_discharge"] / properties_df["capacity_charge"]
    )

    # Write metadata to JSON
    with metadata_file.open("w") as f:
        json.dump(ds.attrs, f, indent=2)

    # Write properties to file
    properties_df.to_csv(summary_file.with_suffix(".csv"))

    # Xarray HVPlot interactive
    num_cycles = len(pd.unique(ds["cycle"]))
    if num_cycles > 10:
        ds_plot = ds.where(ds["cycle"] % 10 == 0)  # Plot every 10th cycle only
    else:
        ds_plot = ds  # Plot all cycles
    num_cycles_new = len(pd.unique(ds["cycle"]))
    legend = True
    if num_cycles_new > 5:
        legend = False  # Disable legend for too many cycles
    cmap = hv.Cycle(sns.color_palette("mako_r", num_cycles_new).as_hex())
    hv_obj0 = ds_plot.hvplot.line(
        x=q_charge_key, y=e_key, by="cycle", legend=False, color=cmap
    )
    hv_obj1 = ds_plot.hvplot.line(
        x=q_discharge_key, y=e_key, by="cycle", legend=legend, color=cmap
    )
    hv_obj = hv_obj0 * hv_obj1

    if num_cycles > 10:
        hv2_obj0 = properties_df.hvplot.scatter(
            y="capacity_charge",
            label="charge",
            show_legend=True,
            xlabel="cycle",
            ylabel=f"capacity ({ds[q_charge_key].units})",
            marker="triangle-up",
        )
        hv2_obj1 = properties_df.hvplot.scatter(
            y="capacity_discharge",
            label="discharge",
            show_legend=True,
            xlabel="cycle",
            ylabel=f"capacity ({ds[q_charge_key].units})",
            marker="triangle-down",
        )
        # hv2_obj2 = properties_df.hvplot.line(
        #     y="coulombic_efficiency",
        #     label="coulombic efficiency",
        #     show_legend=True,
        #     xlabel="cycle",
        #     ylabel="coulombic efficiency (-)",
        # )
        hv2_obj = hv2_obj0 * hv2_obj1  # * hv2_obj2

    # # Save interactive plot to json
    # app = dash.Dash("kadi_plot_preview")
    # dash_components = to_dash(app, hv_obj, reset_button=True)
    # for i, c in enumerate(dash_components.children):
    #     json_string = pio.to_json(c)
    #     with dashboard_file.with_suffix(f".{i:02d}.json").open("w") as f:
    #         f.write(json_string)

    # Save plots to static files
    hvplot.save(hv_obj, figure_file.with_suffix(".svg"))
    hvplot.save(hv_obj, figure_file.with_suffix(".png"))
    if num_cycles > 10:
        hvplot.save(hv2_obj, summary_file.with_suffix(".svg"))
        hvplot.save(hv2_obj, summary_file.with_suffix(".png"))
        print("Done")


def _depaginate_response(paginated_response_function, *args, **kwargs):
    response = paginated_response_function(*args, page=1, **kwargs)
    items = response.json()["items"]
    total_pages = response.json()["_pagination"]["total_pages"]
    for p in range(2, total_pages + 1):
        new_response = paginated_response_function(*args, page=p, **kwargs)
        items.extend(new_response.json()["items"])
    return items


def _generate_markdown_description(file_name, record_id, file_id, title=""):
    title = title or file_name
    row = f"\n\n## {title}\n\n"
    row += f"![{file_name}](/api/records/{record_id}/files/{file_id}/preview/file)"
    return row


if __name__ == "__main__":

    instance = "polis"
    group_identifier = "admins-polis-referencesystem-part-1"
    report_identifier = "polis-referencesystem-part-1-report"
    tag = "analyzed"
    ignore_tags = True  # FIXME
    workdir = Path("tmp")
    file_glob = "*.mpr"
    metadata_glob = f"{metadata_prefix}_*.json"

    # Find records in group based on criteria
    manager = kadi_apy.CLIKadiManager(instance=instance)
    group = manager.group(identifier=group_identifier)
    records = [
        manager.record(id=r["id"]) for r in _depaginate_response(group.get_records)
    ]
    # records = [
    #     manager.record(id=22386)
    # ]  # DEBUG 1b03 22281 1a03 22190 1c03 22384 1c05 22386
    if not ignore_tags:
        records = [r for r in records if tag not in r.get_tags()]

    # Prepare report record
    report_record = manager.record(
        identifier=report_identifier, title=report_identifier, create=True
    )
    report_record.set_attribute(type="report")
    report_record.add_group_role(group, "admin")
    report = ""

    # Loop over records
    for record in records:
        print(f"Processing Record {record.meta['identifier']}")
        # Download files
        record_dir = workdir / record.meta["identifier"]
        record_dir.mkdir(exist_ok=True, parents=True)
        record.get_file(str(record_dir), pattern=file_glob, force=True)
        # Convert mpr files
        mpr_files = list(record_dir.glob(file_glob))
        if not mpr_files:
            print(f"- No mpr files found for {record.meta['identifier']}")
            continue
        for mpr in mpr_files:
            print(f"- Converting file {mpr}")
            try:
                extract_biologic_mpr(mpr)
            except Exception as e:
                print(f"- Error processing {mpr}: {e}")
                continue
        # Upload new files to record
        new_files = sorted(set(record_dir.glob("*")) - set(mpr_files))
        uploaded_figures = []
        for new_file in new_files:
            record.upload_file(new_file, force=True)
            # Memorize uploaded figures
            if (
                new_file.stem.startswith(figure_prefix)
                or new_file.stem.startswith(summary_prefix)
            ) and new_file.suffix == ".png":
                file_id = record.get_file_id(new_file.name)
                uploaded_figures.append((new_file.name, record.id, file_id))
        # Update record metadata
        metadata_files = list(record_dir.glob(metadata_glob))
        metadata = {}
        for mf in metadata_files:
            with mf.open("r") as f:
                metadata[mf.stem] = json.load(f)
        json_kadi = json_to_kadi({auto_key: metadata})
        print(f"- Updating metadata for record {record.meta['identifier']}")
        record.remove_metadatum(auto_key)
        record.add_metadata(json_kadi, force=True)
        record.add_tag(tag)
        record.link_record(report_record, name="reported_in")
        # Update auto-generated report and record description
        print("- Updating auto-generated report and description.")
        report += f"\n\n# {record.meta['identifier']}\n\n"
        if uploaded_figures:
            description = record.meta.get("description", "")
            description = description.split(comment_char + auto_key, 1)[0]
            description += f"\n\n{comment_char}{auto_key}"
            for uf in uploaded_figures:
                title = uf[0].removeprefix(figure_prefix + "_").rsplit(".", 1)[0]
                row = _generate_markdown_description(*uf, title=title)
                description += row
                report += row
            record.set_attribute(description=description)

    # Update report record description
    report_record.set_attribute(description=report)
