import click_completion
from kadi_apy.lib.exceptions import KadiAPYInputError
from xmlhelpy import Bool
from xmlhelpy import Choice
from xmlhelpy import Float
from xmlhelpy import group
from xmlhelpy import Integer
from xmlhelpy import option
from xmlhelpy import TokenList

click_completion.init()

@group()
def electrochemical():
  """Main group for electrochemical analysis."""
  
@electrochemical.group()
def cycler():
  """Cycler data analysis."""
  
@cycler.command("extract", description="Extract data from a cycler output file.")
@option("file", description="The file to extract data from.", required=True)
@option("mass", description="Specific mass in [g]", required=False, param_type=Float, default=None)
@option("filetype", description="File type." required=False, default="biologic_mpr")
def extract_cycling_data(file, mass=None, filetype="biologic_mpr"):
  """Extract data from a file."""
  from .extract import extract_biologic_mpr
  if filetype == "biologic_mpr":
    extract_biologic_mpr(file=file, mass=mass)
  else:
    raise KadiAPYInputError(f"Filetype {filetype} not supported.")