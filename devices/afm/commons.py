import pandas as pd
import re
import os
import pathlib
import glob
import csv

from kadi_apy import KadiManager


# extract experiment method

def extract_experiment(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    experiment = lines[1].strip()

    # extract start_word and end_word
    experiment_dict = {
        "Amperometric": ("Note:", "Time/sec"),
        "Chronopotentiometry": ("Note:", "Time/sec"),
        "Cyclic Voltammetry": ("Note:", "Segment 1:"),
        "Multi-Potential Steps": ("Note:", "Time/sec")
    }

    if experiment in experiment_dict:
        start_word, end_word = experiment_dict[experiment]
    else:
        print("Experiment type not recognized")

    extract_experiment_values = {"experiment":experiment, "start_word": start_word, "end_word":end_word}
    
    return extract_experiment_values


def extract_extra_metadata_1(file_path):
    extract_experiment_values = extract_experiment(file_path)
    experiment = extract_experiment_values['experiment']
    start_word = extract_experiment_values['start_word']
    end_word = extract_experiment_values['end_word']
    last='Header'
    # Read the text file
    with open(file_path, 'r') as f:
        text = f.read()


    pattern = re.compile(f'{experiment}(.*?){last}', re.DOTALL)
    match = pattern.search(text)
    extracted_text = match.group(1).strip()
    lines = extracted_text.split('\n')

    # Split each line into two columns based on a sign
    sign = ':'  # Change this to any other sign you want
    data = []
    for line in lines:
        if sign in line:
            data.append(line.split(sign, 1))
        else:
            data.append([line, ''])

    # Create a DataFrame with two columns
    df_extra_meta_1 = pd.DataFrame(data, columns=['key', 'value'])
    df_extra_meta_1["type"] = "str"
    dict_extra_meta_1 = df_extra_meta_1.to_dict(orient='records')

    return dict_extra_meta_1

# print(extract_extra_metadata_1(file_path))



def extra_metadata_2(file_path):
    extract_experiment_values = extract_experiment(file_path)
    experiment = extract_experiment_values['experiment']
    start_word = extract_experiment_values['start_word']
    end_word = extract_experiment_values['end_word']

    with open(file_path, 'r') as f:
        text = f.read()
    pattern = re.compile(f'{start_word}(.*?){end_word}', re.DOTALL)
    match = pattern.search(text)
    extracted_text = match.group(1).strip()
    lines = extracted_text.split('\n')

    # Split each line into two columns based on a sign
    sign = '='  # Change this to any other sign you want
    data = []
    for line in lines:
        if sign in line:
            data.append(line.split(sign, 1))
        else:
            data.append([line, ''])

    # Create a DataFrame with two columns
    df_extra_meta_2 = pd.DataFrame(data, columns=['key', 'value'])
    df_extra_meta_2_float = df_extra_meta_2[pd.to_numeric(df_extra_meta_2['value'], errors='coerce').notnull()]
    df_extra_meta_2_float_mod= df_extra_meta_2_float.copy()
    df_extra_meta_2_float_mod[['key', 'unit']] = df_extra_meta_2_float_mod['key'].str.split("(", expand=True)
    df_extra_meta_2_float_mod["unit"] = df_extra_meta_2_float_mod["unit"].str.replace(")", "", regex=False)
    df_extra_meta_2_float_mod["type"] = "float"
    df_extra_meta_2_float_mod['value'] = df_extra_meta_2_float_mod['value'].astype(float)
    dict_extrametadata_2_floats = df_extra_meta_2_float_mod.to_dict(orient='records')
    diff = df_extra_meta_2.merge(df_extra_meta_2_float, how='outer', indicator=True)
    # extra metadata 2 strings
    # select the rows that are unique to either DataFrame or have different values in the corresponding columns
    df_extra_meta_2_str = diff[diff['_merge'] != 'both']
    df_extra_meta_2_str = df_extra_meta_2_str.drop("_merge", axis=1)
    df_extra_meta_2_str["type"] = "str"
    dict_extrametadata_2_str = df_extra_meta_2_str.to_dict(orient='records')
    
    return dict_extrametadata_2_floats, dict_extrametadata_2_str


column_values_dict = {"Amperometric": "Time/sec, Current/A",
    "Chronopotentiometry": "Time/sec, Potential/V",
    "Cyclic Voltammetry": "Potential/V, Current/A, Charge/C",
    "Multi-Potential Steps": "Time/sec, Current/A"}

def extract_csv(file_path):
    try:
        extract_experiment_values = extract_experiment(file_path)
        experiment = extract_experiment_values['experiment']

        if experiment in column_values_dict:
            word = column_values_dict[experiment]
        else:
            print("Experiment type not recognized")
        with open(file_path, 'r') as f:
            reader = csv.reader(f, delimiter=',')
            values = []
            for row in reader:
                # Skip empty rows
                if not row:
                    continue
                # Extract the values from the row
                values.append([v for v in row])
            original_list = values
            columns = word.split(",")
            df_csv = pd.DataFrame(original_list, columns=columns)
            # locate the index of the first row to keep
            if experiment == "Cyclic Voltammetry":
                index_locate = (df_csv.loc[df_csv[columns[0]] == columns[0]].index[0] + 2)
            else:
                index_locate = (df_csv.loc[df_csv[columns[0]] == columns[0]].index[0] + 1)
            df_csv = df_csv.loc[index_locate:]
            file_name_download = file_path.strip('txt') + experiment + '.txt'
            temp_folder = (os.path.dirname(file_path) + "/temp")
            temp_path = (os.path.dirname(file_path) + "/temp/" + experiment + ".csv")
            if not os.path.exists(temp_folder):
                os.makedirs(temp_folder)
                
            df_csv.to_csv(temp_path, index=False)

    except Exception as e:
        print("Error in extract_csv:", e)
    
    return temp_path


def upload_extra_metadata_kadi(file_path,identifier,title):
    dict_extra_meta_1 = extract_extra_metadata_1(file_path)
    dict_extrametadata_2_floats, dict_extrametadata_2_str = extra_metadata_2(file_path)

    manager = KadiManager()
    m1 = manager.record(identifier=identifier, title=title, create="True")
    m1.add_metadata(dict_extra_meta_1, force=True, callback = None)
    m1.add_metadata(dict_extrametadata_2_floats, force=True, callback = None)
    m1.add_metadata(dict_extrametadata_2_str, force=True, callback = None)

    return print("Uploaded metadata to kadi.")

def upload_files_kadi(file_path,identifier,title):
    temp_path = extract_csv(file_path)
    manager = KadiManager()
    m1 = manager.record(identifier=identifier, title=title, create="True")
    m1.upload_file(temp_path, force=True)

    return print("Uploaded files to kadi.")

def run(file_path, identifier,title):
    upload_extra_metadata_kadi(file_path,identifier,title)
    upload_files_kadi(file_path,identifier,title)
    return print("Done!")

