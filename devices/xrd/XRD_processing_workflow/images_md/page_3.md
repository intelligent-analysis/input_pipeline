# Step by Step guide for users of the XRD instrument




## Page 3

4) Check for leakage and unusual optical behavior of the XRD instrument.
5) Check voltage and current setting of your X-Ray source
        ◦ Mo: 50kV & 40mA
        ◦ Ag: 40kV & 40mA 
6) If the XRD is completely shut down or shows values like 0kV, 0mA please contact one of the operators.
7) If the instrument is in standby (20kV, 5mA) increase the values of your source stepwise to operation setting. Always increase voltage first, current second. The operation values are also displayed in between the generator and the cabinet window (white label). 
        ◦ Mo: 20kV, 5mA --> 40kV,5mA --> 40kV, 25mA --> 50kV, 25mA --> 50kV, 40mA
        ◦ Ag: 20kV, 5mA --> 35kV,5mA --> 35kV, 25mA --> 40kV, 25mA --> 40kV, 40mA




