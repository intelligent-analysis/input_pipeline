## Step by Step guide for users of the XRD instrument.


### 1.  Before you use the XRD, book the respective device (XRD Mo- or Ag-source) on the Clustermarket website.

### If you cannot book the instrument contact one of the instrument operators (Adam Reupert or Venkat Pamidi - phone numbers on the XRD form) and do not continue with your measurement until this issue is resolved.


### 2. If possible, prepare your sample in the chemistry lab prior to use to not waste any measurement time. 

### See preparation guide for this task.


