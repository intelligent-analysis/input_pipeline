# Step by Step guide for users of the XRD instrument




## Page 5


9) If the instrument was in standby, wait 10 minutes before doing any measurements to minimize fluctuations in the primary X-Ray beam.
10) Log into the PC (user account)
11) Open the instrument software WinXPow --> Diffract. 1/ Diffractometer control
12) Perform the following measurements always after checking each time that the cabin windows are closed (windows itself and green point light next to the emergency shut off), no objects/cables are blocking the detector/sample holder path, the sample holder spins and the correct collimator is used.
13) If the sample spinning won’t start, recheck that the software is open and for transmission holder that the gold pins have contact to the sample holder.
14) If measuring in transmission mode check if scan mode (M) is set to Transmission and Scan type is 2theta:omega.
15) Optional: Check the beam stop by performing a Step Scan (S) at 0° for 1s at a detector width of 1001 NSteps (~+-7.5°). Step Scan should result in an error message telling that no peak was found. 
16) Insert the LaB6 standard carefully from the references box into the sample holder and move it to ~ 0°. Start the sample spinning and perform a Step Scan (S) at 13.860° for Mo and 10.921° for Ag with a time setting of 10s and 101 NSteps.
        ◦ For the transmission sample: 
            i. left means for Ag, right means for Mo side
            ii. The orientation of the sample disc should be that the shiny metal surface heads towards the primary beam 
