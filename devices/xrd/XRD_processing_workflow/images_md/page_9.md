# Step by Step guide for users of the XRD instrument




## Page 9


20) Put the LaB6 standard back in the references box in the cabinet!
21) Safe all settings that you are now doing in a measurement file by the tab File/Save as and choosing a save location.
22) Click on scan mode (M) and check:
        ◦ For transmission measurement: 
            i. Scan Mode: Transmission
            ii. PSD mode: Moving
            iii. Omega mode: Fixed 
            iv. Scan type: 2Theta:Omega
        ◦ For capillary measurement: 
            i. Scan mode: Debye-Scherrer
            ii. PSD mode: Moving 
            iii. Omega mode: Fixed (gray/not editable)
            iv. Scan type should 2Theta and gray/not editable
23) Click on Ranges and define your ranges. Smaller step scan than 2.004 won’t give you better resolution. Rather increase your measurement time for higher sensitivity!
24) Click on Scan usage (U)
        ◦ For only one sample measured for short time select single sample
        ◦ For one sample measured long time select repetition
            i. Number of repetitions: self-explanatory
            ii. Wait time: Time the detector waits between measurements: Set minimum to 1 min  
        ◦ For multiple samples select multiple samples and check individual ranges checkbox
            i. Specify the amount of samples you want to measure (max. 30)
            ii. Specify which sample should use which range from the range menu. Zero means all ranges are measured for this sample. Also select a file name with the .raw ending so you are able to directly open your measured samples with the WinXPow software.
25) After all your settings are done recheck the measurement cabin one last time and click on data collection (C).
26) If nobody uses the instrument after you on this day (Clustermarket) and it’s also your last measurement, select the “set instrument to standby after measurement” option and start the measurement.
27) Always wait until the shutter opens (Green light switches to red) and you see the first counts of your measurement in the software, then put the PC in sleep mode.  
28) After your measurement is done stop the sample spinning, remove the sample from the cabinet, close the measurement file and enter your end date/ end time in the XRD form and sign it.
