# Step by Step guide for users of the XRD instrument




## Steps

1) Before you use the XRD, book the respective device (XRD Mo- or Ag-source) on the Clustermarket website. If you cannot book the instrument contact one of the instrument operators (Adam Reupert or Venkat Pamidi - phone numbers on the XRD form) and do not continue with your measurement until this issue is resolved.
2) If possible, prepare your sample in the chemistry lab prior to use to not waste any measurement time. See preparation guide for this task.
3) ## _Enter your name and the start time in the XRD form_.
4) Check for leakage and unusual optical behavior of the XRD instrument.
5) Check voltage and current setting of your X-Ray source
        ◦ Mo: 50kV & 40mA
        ◦ Ag: 40kV & 40mA 
6) If the XRD is completely shut down or shows values like 0kV, 0mA please contact one of the operators.
7) If the instrument is in standby (20kV, 5mA) increase the values of your source stepwise to operation setting. Always increase voltage first, current second. The operation values are also displayed in between the generator and the cabinet window (white label). 
        ◦ Mo: 20kV, 5mA --> 40kV,5mA --> 40kV, 25mA --> 50kV, 25mA --> 50kV, 40mA
        ◦ Ag: 20kV, 5mA --> 35kV,5mA --> 35kV, 25mA --> 40kV, 25mA --> 40kV, 40mA
8) ## _Check the cooling water and enter the value in the XRD form_.
9) If the instrument was in standby, wait 10 minutes before doing any measurements to minimize fluctuations in the primary X-Ray beam.
10) Log into the PC (user account)
11) Open the instrument software WinXPow --> Diffract. 1/ Diffractometer control
12) Perform the following measurements always after checking each time that the cabin windows are closed (windows itself and green point light next to the emergency shut off), no objects/cables are blocking the detector/sample holder path, the sample holder spins and the correct collimator is used.
13) If the sample spinning won’t start, recheck that the software is open and for transmission holder that the gold pins have contact to the sample holder.
14) If measuring in transmission mode check if scan mode (M) is set to Transmission and Scan type is 2theta:omega.
15) Optional: Check the beam stop by performing a Step Scan (S) at 0° for 1s at a detector width of 1001 NSteps (~+-7.5°). Step Scan should result in an error message telling that no peak was found. 
16) Insert the LaB6 standard carefully from the references box into the sample holder and move it to ~ 0°. Start the sample spinning and perform a Step Scan (S) at 13.860° for Mo and 10.921° for Ag with a time setting of 10s and 101 NSteps.
        ◦ For the transmission sample: 
            i. left means for Ag, right means for Mo side
            ii. The orientation of the sample disc should be that the shiny metal surface heads towards the primary beam 
17) ## _Enter the measured Imax in the XRD form and specify the used collimator (for transmission always short!)_. 
18) Close the StepScan window.
19) ## _Enter the material name you want to measure in the form and place it in the sample holder/sample tube. When using the sample tube be aware that the first sample you insert is the last sample measured_. 
20) Put the LaB6 standard back in the references box in the cabinet!
21) Safe all settings that you are now doing in a measurement file by the tab File/Save as and choosing a save location.
22) Click on scan mode (M) and check:
        ◦ For transmission measurement: 
            i. Scan Mode: Transmission
            ii. PSD mode: Moving
            iii. Omega mode: Fixed 
            iv. Scan type: 2Theta:Omega
        ◦ For capillary measurement: 
            i. Scan mode: Debye-Scherrer
            ii. PSD mode: Moving 
            iii. Omega mode: Fixed (gray/not editable)
            iv. Scan type should 2Theta and gray/not editable
23) Click on Ranges and define your ranges. Smaller step scan than 2.004 won’t give you better resolution. Rather increase your measurement time for higher sensitivity!
24) Click on Scan usage (U)
        ◦ For only one sample measured for short time select single sample
        ◦ For one sample measured long time select repetition
            i. Number of repetitions: self-explanatory
            ii. Wait time: Time the detector waits between measurements: Set minimum to 1 min  
        ◦ For multiple samples select multiple samples and check individual ranges checkbox
            i. Specify the amount of samples you want to measure (max. 30)
            ii. Specify which sample should use which range from the range menu. Zero means all ranges are measured for this sample. Also select a file name with the .raw ending so you are able to directly open your measured samples with the WinXPow software.
25) After all your settings are done recheck the measurement cabin one last time and click on data collection (C).
26) If nobody uses the instrument after you on this day (Clustermarket) and it’s also your last measurement, select the “set instrument to standby after measurement” option and start the measurement.
27) Always wait until the shutter opens (Green light switches to red) and you see the first counts of your measurement in the software, then put the PC in sleep mode.  
28) After your measurement is done stop the sample spinning, remove the sample from the cabinet, close the measurement file and enter your end date/ end time in the XRD form and sign it.

