# General plotting ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import matplotlib.pyplot as plt
import scienceplots
import pandas as pd
import numpy as np
import sys
import ast
import json

#ECLab-Files ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import eclabfiles as ecf

#XRD-Files ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import re
import dateutil.parser as date_parser
import os

#Animation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from matplotlib.animation import FuncAnimation
from matplotlib.animation import PillowWriter
from IPython.display import Markdown as md

#Multiplot from figures ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import cv2

#Kadi4mat ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import kadi_apy
from kadi_apy import CLIKadiManager
manager = CLIKadiManager()


    
kadi_users = {}
kadi_groups = {}
kadi_collections = {}
kadi_records = {}
kadi_elements = [kadi_users, kadi_groups, kadi_collections, kadi_records]




def add_record(identifier, title, record_type, description = None, add_tags = None, remove_tags = None, fileloc = None, filedesc = None, metadata = None, collection_link = None, record_link = None, users=None, groups=None):
    record = manager.record(identifier=identifier, title=title, create=True)
    record.edit(type = record_type, description = description)
    for i in add_tags:
        record.add_tag(i)
    for i in remove_tags:
        record.remove_tag(i)
    file_number = 0
    for i in fileloc:
        print('Upload ' + str(filedesc[file_number]) + ' from ' + str(fileloc[file_number]))
        record.upload_file(fileloc[file_number],file_description = filedesc[file_number])
        file_number = file_number + 1
    record.add_metadata(metadata, force=True)
    # Link to collections
    for i in collection_link:
        record.add_collection_link(i)
    # Link to records
    for i in record_link:
        i[0].link_record(record, name = i[1])
    for i in users:
        record.add_user(i[0],permission_new=i[1])
    for i in groups:
        record.add_group_role(i[0],permission_new=i[1])
    return record




def xrd_processing(filepath, metadata_head_export = True, range_txt_export = True, transpose = True, combine = True, plot = True, cleanup = False, comments = False, kadi4mat_record_infos = None, kadi4mat = False):
    """
    Function loads the given .txt full report file, extracts the parameter, 
    combines the individual detector measurements and creates a combined range-measurement 
    as well as combines the measurement if parameters line up.
    
    :param str filepath: path to the file that should be plotted
    :param bool metadata_head_export: Exports header of XRD file containing most metadata.
    :param bool range_txt_export: Exports data of individual ranges as individual .txt files
    :param bool transpose: Transposes weird data format of the XRD files to a xy file and saves the files for each range as csv file individually
    :param bool combine: Creates a .csv file containing data of all ranges including one column with added ranges, one with combined ranges and normalized ranges as well.
    :param bool plot: Creates one plot of all measured ranges above each other and one containing the combined, added and normalized data. 
    :param bool kadi4mat: Uploads created files to the Kadi4mat instance.
    :param bool cleanup: Deletes all files created after the processing
    :param bool comments: prints additional infos while processing the data.
    """
    
    #print('Start processing')
    xrd_metadata = []
    generated_data_files = []
    generated_data_files.append(filepath[:-3]+'raw')
    generated_data_files.append('Raw data measurement file. Unmodified derived from instrument.')
    generated_data_files.append(filepath[:-3]+'txt')
    generated_data_files.append('Data measurement file. Only task was utilization of the instrument software export function. No other/further modifications!')
    #import re
    
    
    #Splitting at Range keyword leading to metadata at files[0] and individual ranges in files[i]
    with open(filepath, 'r') as f:
        files = f.read().split('Range ')
        f.close()
        
    #Extracting metadata from first header (files[0])
    
    #File_parameters:
    regex_search = re.findall('File\s*\:\s*(\S+.*)', files[0], re.I)
    if regex_search:
        file_parameter_name = regex_search[0]
    #print(file_parameter_name)
    
    regex_search = re.findall('Created\s*\:\s*(\S+.+?(?=by))', files[0], re.I)
    if regex_search:
        file_parameter_created = regex_search[0]
    #print(file_parameter_created)
    file_parameter_created_iso = date_parser.parse(file_parameter_created).isoformat()
    #print(file_parameter_created_iso)

    regex_search = re.findall('by\s*(\S+.*)', files[0], re.I)
    if regex_search:
        file_parameter_created_by = regex_search[0]
    #print(file_parameter_created_by)
    
    regex_search = re.findall('Title\s*\:\s*(\S+.*)', files[0], re.I)
    if regex_search:
        file_parameter_title = regex_search[0]
    #print(file_parameter_title)
    
    #Instrument_parameters:
    regex_search = re.findall('Diffract.\s*\:\s*(\S+)', files[0], re.I)
    if regex_search:
        instrument_parameter_mode = regex_search[0]
    #print(instrument_parameter_mode)
    
    regex_search = re.findall('Monochrom.\s*\:\s*(\S+.*)', files[0], re.I)
    if regex_search:
        instrument_parameter_monochromator = regex_search[0]
    #print(instrument_parameter_monochromator)
    
    regex_search = re.findall('Radiation\s*\:\s*(\S+\s*\S+)', files[0], re.I)
    if regex_search:
        instrument_parameter_radiation = regex_search[0]
    #print(instrument_parameter_radiation)
    
    regex_search = re.findall('Generator\s*\:\s*(\S+)\s*kV,\s*(\S+)', files[0], re.I)
    if regex_search:
        instrument_parameter_generator = regex_search[0]
    #print(instrument_parameter_generator[0])
    #print(instrument_parameter_generator[1])
    
    regex_search = re.findall('Detector\s*\:\s*(\S+)', files[0], re.I)
    if regex_search:
        instrument_parameter_detector = regex_search[0]
    #print(instrument_parameter_detector)
    
    regex_search = re.findall('PSD Mode\s*\:\s*(\S+.*)', files[0], re.I)
    if regex_search:
        instrument_parameter_psd_mode = regex_search[0]
    #print(instrument_parameter_psd_mode)
    
    #General measurement_parameters:
    regex_search = re.findall('Scan Mode\s*\:\s*(\S+.+?(?=Scan Type))', files[0], re.I)
    if regex_search:
        measurement_parameter_scan_mode = regex_search[0]
    #print(measurement_parameter_scan_mode)
    
    regex_search = re.findall('Scan Type\s*\:\s*(\S+.*)', files[0], re.I)
    if regex_search:
        measurement_parameter_scan_type = regex_search[0]
    #print(measurement_parameter_scan_type)
    
    regex_search = re.findall('Scan Usage\s*\:\s*(\S+.+?(?=Pts. added))', files[0], re.I)
    if regex_search:
        measurement_parameter_scan_usage = regex_search[0]
    #print(measurement_parameter_scan_usage)
    
    regex_search = re.findall('Pts. added\s*\:\s*(\S+.*)', files[0], re.I)
    if regex_search:
        measurement_parameter_points_added = regex_search[0]
    #print(measurement_parameter_points_added)
    
    regex_search = re.findall('Number of ranges\s*\:\s*(\S+),\s*measured\s*(\S+)', files[0], re.I)
    if regex_search:
        measurement_parameter_range = regex_search[0]
    #print(measurement_parameter_range[0])
    #print(measurement_parameter_range[1])
    
    regex_search = re.findall('Applied data corrections:\n*\s*(\S+.*)\n*\s*(\S+.*)', files[0], re.I)
    if regex_search:
        measurement_parameter_correction = regex_search[0]
    #print(measurement_parameter_correction[0][2:])
    #print(measurement_parameter_correction[1][2:])
 
    
    xrd_metadata.append({"key": "File parameter", "type": "dict", "value":
                            [
                            {"key": "File", "type": "str", "value": str(file_parameter_name)},
                            {"key": "Title", "type": "str", "value": str(file_parameter_title)},
                            {"key": "Created", "type": "date", "value": str(file_parameter_created_iso)},
                            {"key": "Created by", "type": "str", "value": str(file_parameter_created_by)},
                            ]}
                       )
    xrd_metadata.append({"key": "Instrument parameter", "type": "dict", "value":
                            [
                            {"key": "Mode", "type": "str", "value": str(instrument_parameter_mode)},
                            {"key": "Monochrom.", "type": "str", "value": str(instrument_parameter_monochromator)},
                            {"key": "Radiation", "type": "str", "value": str(instrument_parameter_radiation)},                    
                            {"key": "Generator", "type": "dict", "value": 
                                [
                                {"key": "Voltage", "type": "int", "unit": "kV", "value": int(instrument_parameter_generator[0])},
                                {"key": "Current", "type": "int", "unit": "mA", "value": int(instrument_parameter_generator[1])},
                                ]},
                            {"key": "Detector", "type": "str", "value": str(instrument_parameter_detector)},
                            {"key": "PSD Mode", "type": "str", "value": str(instrument_parameter_psd_mode)},
                            ]}
                       )

     
    
    range_parameter_settings =  [
                                {"key": "Set", "type": "int", "unit": "ranges", "value": int(measurement_parameter_range[0])},
                                {"key": "Measured", "type": "int", "unit": "ranges", "value": int(measurement_parameter_range[1])},
                                ]
    
#Range specific measurement_parameters:
    for x in range(1, int(measurement_parameter_range[1])+1):
        regex_search = re.findall('2Theta \(begin, end, step\)\s*\:\s*(\S+)\s*(\S+)\s*(\S+)', files[x], re.I)
        if regex_search:
            measurement_parameter_TwoTheta = regex_search[0]
        #print(measurement_parameter_TwoTheta[0])
        #print(measurement_parameter_TwoTheta[1])
        #print(measurement_parameter_TwoTheta[2])

        regex_search = re.findall('Omega  \(begin, end, step\)\s*\:\s*(\S+)\s*(\S+)\s*(\S+)', files[x], re.I)
        if regex_search:
            measurement_parameter_Omega = regex_search[0]
        #print(measurement_parameter_Omega[0])
        #print(measurement_parameter_Omega[1])
        #print(measurement_parameter_Omega[2])

        regex_search = re.findall('Sample position\s*\:\s*(\S+)\s*(\S+)', files[x], re.I)
        if regex_search:
            measurement_parameter_sample_position = regex_search[0]
        #print(measurement_parameter_sample_position[0])
        #print(measurement_parameter_sample_position[1])

        regex_search = re.findall('Time/Step\s*\:\s*(\S+)\s*s\s*PSD Step\:\s*(\S+)\s*(\S+)\s*s/step', files[x], re.I)
        if regex_search:
            measurement_parameter_detector_settings = regex_search[0]
        #print(measurement_parameter_detector_settings[0])
        #print(measurement_parameter_detector_settings[1])
        #print(measurement_parameter_detector_settings[2])

        #Data collection  : 13-Mar-23 17:15 .. 13-Mar-23 18:14
        regex_search = re.findall('Data collection\s*\:\s*(\S+\s*\S+)\s*..\s*(\S+\s*\S+)', files[x], re.I)
        if regex_search:
            measurement_parameter_data_collection = regex_search[0]
        measurement_parameter_data_collection_iso = []
        measurement_parameter_data_collection_iso.append(date_parser.parse(measurement_parameter_data_collection[0]).isoformat())
        measurement_parameter_data_collection_iso.append(date_parser.parse(measurement_parameter_data_collection[1]).isoformat())
        #print(measurement_parameter_data_collection[0])
        #print(measurement_parameter_data_collection[0])
        #print(measurement_parameter_data_collection_iso[0])
        #print(measurement_parameter_data_collection_iso[1])

        regex_search = re.findall('Number of points\s*\:\s*(\S+)\s*Min. and max. intensity\s*\:\s*(\S+),\s*(\S+)\s*', files[x], re.I)
        if regex_search:
            measurement_parameter_range_infos = regex_search[0]
        #print(measurement_parameter_range_infos[0])
        #print(measurement_parameter_range_infos[1])
        #print(measurement_parameter_range_infos[2])
    
        range_parameter_settings.append({"key": "Range " + str(x) , "type": "dict", "value":
                                            [
                                            {"key": "2Theta", "type": "dict", "value":
                                                [
                                                {"key": "Begin", "type": "float", "unit": "°", "value": float(measurement_parameter_TwoTheta[0])},
                                                {"key": "End", "type": "float", "unit": "°", "value": float(measurement_parameter_TwoTheta[1])},
                                                {"key": "Step", "type": "float", "unit": "°", "value": float(measurement_parameter_TwoTheta[2])},
                                                ]},
                                            {"key": "Omega", "type": "dict", "value":
                                                [
                                                {"key": "Begin", "type": "float", "unit": "°", "value": float(measurement_parameter_Omega[0])},
                                                {"key": "End", "type": "float", "unit": "°", "value": float(measurement_parameter_Omega[1])},
                                                {"key": "Step", "type": "float", "unit": "°", "value": float(measurement_parameter_Omega[2])},
                                                ]},
                                            {"key": "Sample position", "type": "dict", "value":
                                                [
                                                {"key": "2Theta", "type": "float", "unit": "°", "value": float(measurement_parameter_sample_position[0])},
                                                {"key": "Omega", "type": "float", "unit": "°", "value": float(measurement_parameter_sample_position[1])},
                                                ]},
                                            {"key": "Detector settings", "type": "dict", "value":
                                                [
                                                {"key": "Time/Step", "type": "float", "unit": "s", "value": float(measurement_parameter_detector_settings[0])},
                                                {"key": "PSD Step", "type": "float", "unit": "°", "value": float(measurement_parameter_detector_settings[1])},
                                                {"key": "PSD Time", "type": "float", "unit": "s/step", "value": float(measurement_parameter_detector_settings[2])},
                                                ]},
                                            {"key": "Data collection", "type": "dict", "value":
                                                [
                                                {"key": "Start", "type": "date", "value": str(measurement_parameter_data_collection_iso[0])},
                                                {"key": "End", "type": "date", "value": str(measurement_parameter_data_collection_iso[1])},
                                                ]},     
                                            {"key": "Result information", "type": "dict", "value":
                                                [
                                                {"key": "Number of points", "type": "int", "unit": "points", "value": int(measurement_parameter_range_infos[0])},
                                                {"key": "Min. intensity", "type": "int", "unit": "counts", "value": int(measurement_parameter_range_infos[1])},
                                                {"key": "Max. intensity", "type": "int", "unit": "counts", "value": int(measurement_parameter_range_infos[2])},
                                                ]},
                                            ]},
                                        )
    
    xrd_metadata.append({"key": "Measurement parameter", "type": "dict", "value":
                            [
                            {"key": "Scan Mode", "type": "str", "value": str(measurement_parameter_scan_mode)},
                            {"key": "Scan Type", "type": "str", "value": str(measurement_parameter_scan_type)},
                            {"key": "Scan Usage", "type": "str", "value": str(measurement_parameter_scan_usage)},
                            {"key": "Pts. added", "type": "int", "value": int(measurement_parameter_points_added)},
                            {"key": "Data corrections", "type": "list", "value":
                                [
                                {"type": "str", "value": str(measurement_parameter_correction[0][2:])},
                                {"type": "str", "value": str(measurement_parameter_correction[1][2:])},
                                ]},
                            {"key": "Ranges", "type": "dict", "value": range_parameter_settings},
                            ]}
                        ) 
    
    print("extracted metadata")
    
    #Save metadata and individual ranges in separate text files
    if metadata_head_export:
        print("export started")
        text_file = open("xrd_measurement_metadata.txt", "w")
        text_file.write(files[0])
        text_file.close()
        generated_data_files.append("xrd_measurement_metadata.txt")
        generated_data_files.append("Metadata from XRD data file header")
    if range_txt_export:
        for x in range(1, int(measurement_parameter_range[1])+1):
            text_file = open("xrd_measurement_range_" + str(x) + ".txt", "w")
            text_file.write(files[x])
            text_file.close()
            generated_data_files.append("xrd_measurement_range_" + str(x) + ".txt")
            generated_data_files.append("Text export of range " + str(x))
            print("export finished")
    
    #Transpose weird XRD format to a xy format file and save as individual ranges.csv
    if transpose:
        if range_txt_export:
            for x in range(1, int(measurement_parameter_range[1])+1):
                #read text file into pandas DataFrame
                df_loaded = pd.read_table("xrd_measurement_range_" + str(x) + ".txt", delimiter=r"\s+", skiprows=12, header=None)
                #print(df_loaded)

                j = 0
                df_transposed = pd.DataFrame()
                for i in range(df_loaded.shape[0]): # starting from 0 row
                    for k in range(1,df_loaded.shape[1]): # starting from 0 row
                        if np.isnan(df_loaded.iloc[i,k]):
                            break
                        df_transposed.at[j,'TwoTheta'] = round(float(measurement_parameter_TwoTheta[0])+j*float(measurement_parameter_TwoTheta[2]),3)
                        df_transposed.at[j,'Intensity'] = df_loaded.iloc[i,k]
                        j = j+1
                df_transposed.to_csv("xrd_measurement_range_" + str(x) + ".csv", index = False, sep = ',', decimal='.')
                #print(df_transposed)
                generated_data_files.append("xrd_measurement_range_" + str(x) + ".csv")
                generated_data_files.append("Range " + str(x) + " transformed to xy format and exported as a .csv file")
        else:
            print('Needs range_txt_export set to True')

    #Combine ranges in one file, add a collumn for the averaged and added values
    if combine:
        if transpose:
            df_combined = pd.DataFrame()
            for x in range(1, int(measurement_parameter_range[1])+1):
                Range = 'Range_'+str(x)
                df_transposed = pd.read_table("xrd_measurement_range_" + str(x) + ".csv", delimiter=',', header=None, skiprows = 1)
                df_combined.loc[:, 'TwoTheta'] = df_transposed.iloc[:,0]
                df_combined.loc[:, Range] = df_transposed.iloc[:,1]
            df_combined.loc[:,'Ranges_Added'] = 0
            df_combined.loc[:,'Ranges_Added_normalized'] = 0
            df_combined.loc[:,'Ranges_Averaged'] = 0
            df_combined.loc[:,'Ranges_Averaged_normalized'] = 0
            for i in range(1,df_combined.shape[1]-4): 
                df_combined.loc[:,'Ranges_Added'] = df_combined.loc[:,'Ranges_Added'] + df_combined.iloc[:,i]
            df_combined.loc[:,'Ranges_Added_normalized'] = df_combined.loc[:,'Ranges_Added'] / df_combined.loc[:,'Ranges_Added'].max()
            df_combined.loc[:,'Ranges_Averaged'] = df_combined.loc[:,'Ranges_Added'] / (df_combined.shape[1]-5)
            df_combined.loc[:,'Ranges_Averaged_normalized'] = df_combined.loc[:,'Ranges_Averaged'] / df_combined.loc[:,'Ranges_Averaged'].max()
            print("about to export csv")
            df_combined.to_csv("xrd_measurement_ranges_combined.csv", index = False, sep = ',', decimal='.')
            print("csv exported")
            generated_data_files.append("xrd_measurement_ranges_combined.csv")
            generated_data_files.append("All ranges combined in one .csv file. Added extra columns with averaged, summarized and normalized values.")
        else:
            print('Needs range_txt_export and transpose set to True')
            
    #Plot ranges
    if plot:
        if combine:
            print("plot started")
            fig = plt.figure()
            fig.set_figheight(6)
            fig.set_figwidth(8)
            # Placing the plots in the plane
            plt.style.use(['science','notebook'])
            plot1 = plt.subplot2grid((1, 1), (0, 0), rowspan=1, colspan=1)
            for x in range(1, int(measurement_parameter_range[1])+1):
                # Plotting actual graph
                data = pd.read_csv("xrd_measurement_ranges_combined.csv",sep=',',header=None, skiprows=1)
                data = pd.DataFrame(data)
                plot1.plot(data[0], data[x]-((x-1)*(data[x-1].max())), '-',linewidth=1, label = 'Range_' + str(x))
            plot1.legend(loc="upper right", frameon=True, fontsize="15")
            plot1.set_xlabel('2 Theta / [°]')
            plot1.set_ylabel('Intensity / [a.u.]')
            plot1.set(yticks=[])
            #plot1.set_xlim(xmin = 3, xmax = 30)
            fig.savefig('Plot_XRD_all_ranges.png', dpi=300, bbox_inches='tight')
            generated_data_files.append('Plot_XRD_all_ranges.png')
            generated_data_files.append("All ranges plotted above each other for comparison of outliners.")
            #Plot added
            fig = plt.figure()
            fig.set_figheight(12)
            fig.set_figwidth(16)
            # Placing the plots in the plane
            plt.style.use(['science','notebook'])
            plot1 = plt.subplot2grid((2, 2), (0, 0), rowspan=1, colspan=1)
            # Plotting actual graph
            plot1.plot(data[0], data[data.shape[1]-4], '-',linewidth=1, label = 'Added Ranges')
            plot1.legend(loc="upper right", frameon=True, fontsize="15")
            plot1.set_xlabel('2 Theta / [°]')
            plot1.set_ylabel('Intensity / [a.u.]')
            plot1.set(yticks=[])

            plot2 = plt.subplot2grid((2, 2), (0, 1), rowspan=1, colspan=1)
            plot2.plot(data[0], data[data.shape[1]-2], '-',linewidth=1, label = 'Combined Ranges')
            plot2.legend(loc="upper right", frameon=True, fontsize="15")
            plot2.set_xlabel('2 Theta / [°]')
            plot2.set_ylabel('Intensity / [a.u.]')
            plot2.set(yticks=[])

            plot3 = plt.subplot2grid((2, 2), (1, 0), rowspan=1, colspan=1)
            plot3.plot(data[0], data[data.shape[1]-3], '-',linewidth=1, label = 'Added Ranges normalized')
            plot3.plot(data[0], data[data.shape[1]-1]-1, '-',linewidth=1, label = 'Combined Ranges normalized')
            plot3.legend(loc="upper right", frameon=True, fontsize="15")
            plot3.set_xlabel('2 Theta / [°]')
            plot3.set_ylabel('Intensity / [a.u.]')
            fig.savefig('Plot_XRD_combined_addded.png', dpi=300, bbox_inches='tight')
            generated_data_files.append('Plot_XRD_combined_addded.png')
            generated_data_files.append("Plotted the summarized ranges, combined ranges and normalized ranges for comparison of outliners.")
            print("plot finished")
        else:
            print('Needs range_txt_export, transpose and combine set to True')
       
    if kadi4mat:
            kadi4mat_record_infos= record_infos_dict()
            all_user_inputs = add_metadata_userinput()
            print(kadi4mat_record_infos)
            if kadi4mat_record_infos == None:
                print('Please add a kadi4mat_record_infos dict. variable containing upload infos like the identifier, title, etc. . Until then record can not be uploaded to Kadi4mat instance!')
            else:
                print('Export to Kadi4mat')
                identifier = kadi4mat_record_infos["identifier"]
                title = kadi4mat_record_infos["title"]
                record_type = kadi4mat_record_infos["record_type"]
                description = kadi4mat_record_infos["description"]
                add_tags = kadi4mat_record_infos["add_tags"]
                remove_tags = kadi4mat_record_infos["remove_tags"]
                filepath = kadi4mat_record_infos["fileloc"]
                filedesc = kadi4mat_record_infos["filedesc"]
                metadata = kadi4mat_record_infos["metadata"]
                collection_link = kadi4mat_record_infos["collection_link"]
                record_link = kadi4mat_record_infos["record_link"]
                users = kadi4mat_record_infos["users"]
                groups = kadi4mat_record_infos["groups"]
                
                for i in range (len(generated_data_files)): # files
                    if i % 2 == False:
                        filepath.append(generated_data_files[i])
                for i in range (len(generated_data_files)): # file descriptions
                    if i % 2 == True:
                        filedesc.append(generated_data_files[i])   
                for i in range (len(xrd_metadata)): # metadata
                    metadata.append(xrd_metadata[i])
                metadata.append({"key": "User Inputs", "type": "dict", "value": all_user_inputs})
                xrd_rec = add_record(identifier, title, record_type, description, add_tags, remove_tags, filepath, filedesc, metadata, collection_link, record_link, users, groups)
    
    if cleanup:
        for i in range (4, len(generated_data_files)): # files
            if i % 2 == False:
                os.remove(generated_data_files[i])
    
    if comments:
        print('Metadata:')
        print(metadata)
        print('Created Files:')
        for i in range (len(generated_data_files)):
            print(generated_data_files[i])
    return xrd_rec

def record_infos_dict(): 
    # current_directory = os.getcwd()
    directory_path = '/'.join(filepath.split('/')[:-1])
    filepath_record_infos=directory_path + "/record_infos_filled.txt"
    # file_path = os.path.join(current_directory, "record_infos_filled.txt")
    with open (filepath_record_infos, 'r') as f:
        record_infos = f.read()
        start_index = record_infos.find("The input string is:")

        # Remove "The input string is:" from the string
        result_string = record_infos[start_index + len("The input string is:"):]
        my_list = eval(result_string)
        value_lists = [entry["value"] for entry in my_list]


    # Extract all "key" and "value" pairs
        key_value_pairs = []

        for inner_list in value_lists:
            for entry in inner_list:
                key = entry['key']
                value = entry['value']
                if isinstance (value, list):
                    for j in value:
                        # print(j)
                        value_j = j['value']
                        key_value_pairs.append((key, value_j))
                else:
                    key_value_pairs.append((key, value))


        keys_to_split = ['add_tags', 'remove_tags', 'fileloc', 'filedesc',
                        'metadata','collection_link','record_link', 'users','groups']


        # Initialize an empty dictionary to store the result
        record_infos_dict = {}
        # Iterate through the list of tuples
        for key, value in key_value_pairs:
            # Check if the key is in the list of keys to split
            if key in keys_to_split:
                # Split the value by comma and create a list
                value = value.split(', ')
            # Add the key and value to the dictionary
            record_infos_dict[key] = value
        

        final_dict = {}
        for key, value in record_infos_dict.items():
            if isinstance(value, list):
                final_dict[key] = [] if value == [' '] else value
            else:
                final_dict[key] = value

    return final_dict

def flatten_nested_dict_list(input_list, result=None):
    if result is None:
        result = []

    for item in input_list:
        if isinstance(item, list):
            flatten_nested_dict_list(item, result)
        elif isinstance(item, dict):
            result.append(item)

    return result

def add_metadata_userinput(): 
    # current_directory = os.getcwd()
    directory_path = '/'.join(filepath.split('/')[:-1])
    filepath_userinputs =directory_path + "/user_inputs.txt"
    # file_path = os.path.join(current_directory, "record_infos_filled.txt")
    modified_content = ""
    with open (filepath_userinputs, 'r') as f:
        for line in f:
            modified_line = line.replace("The input string is: ", "")
            for i in modified_line:
                # dictionary = json.loads(i)
                # print(type(i))
                modified_content += i
    # Split the string into lines
    data_lines = modified_content.split('\n')

    # Initialize an empty list to store dictionaries
    data_list = []

    # Iterate over the lines and parse each line as JSON
    for line in data_lines:
        if line.strip():  # Check if the line is not empty
            try:
                json_data = json.loads(line)
                data_list.append(json_data)
            except json.JSONDecodeError as e:
                print(f"Error parsing JSON: {e}")

    all_user_inputs = (flatten_nested_dict_list(data_list))

    return all_user_inputs

filepath = str(sys.argv[1])


if __name__ == "__main__":
    print(record_infos_dict)
    xrd_processing(filepath, metadata_head_export = True, range_txt_export = True, transpose = True, combine = True, plot = True, cleanup = False, comments = False, kadi4mat_record_infos = None, kadi4mat = True)
